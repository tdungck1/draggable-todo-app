const SearchableDropdown = ({ mailList, defaultValue, onChange }) => {
    return <>
        <input type="text" list="data" defaultValue={defaultValue} onChange={onChange} />

        <datalist id="data">
            {mailList.map((item, key) =>
                <option key={key} value={item} />
            )}
        </datalist>
    </>
}

export default SearchableDropdown