import React, { Component } from 'react';
import './App.css';
import { TodoItem } from './todoItem';

function arraymove(arr, fromIndex, toIndex) {
  var element = arr[fromIndex];
  arr.splice(fromIndex, 1);
  arr.splice(toIndex, 0, element);
  return arr
}

export default class App extends Component {
  state = {
    tasks: [],
    newTask: null,
    mailList: ['tdungck1@gmail.com']
  }

  onDragStart = (ev, index) => {
    console.log('dragstart:', index);
    ev.dataTransfer.setData("sourceId", index);
  }

  onDragOver = (ev) => {
    ev.preventDefault();
  }

  onDrop = (ev, targetId) => {
    console.log('onDragOver:', targetId);
    let sourceId = ev.dataTransfer.getData("sourceId");

    let tasks = arraymove(this.state.tasks, sourceId, targetId)

    this.setState({
      tasks
    });
  }

  addMailList = (text) => {
    if(!this.state.mailList.includes(text)) this.setState({mailList: [...this.state.mailList, text]})
  }

  onChange = (changedTask, changedIndex) => {
    let tasks = this.state.tasks.map((task, index) => {
      if (index === changedIndex) return changedTask
      return task
    })
    this.setState({ tasks })
  }
  onNewTaskChange = (changedTask) => {
    this.setState({ newTask: changedTask })
  }

  addNewTask = () => {
    const {newTask} = this.state

    this.addMailList(this.state.newTask?.assignedTo)

    if(!(newTask.title && newTask.assignedTo && newTask.dueDate)) return alert('need to fill on required fields')

    let tasks = [...this.state.tasks, {...newTask, createdOn: new Date()}]
    this.setState({ tasks })
  }

  onDeleteTodo = (index) => {
    let tasks = [...this.state.tasks]
    tasks.splice(index, 1);
    this.setState({tasks})
  }

  render() {
    return (
      <div>
        {this.state.tasks.map((task, index) => {
          return <div
            style={{ marginRight: '5px', padding: '5px', display: 'inline-block', height: 'auto', width: '200px', overflow: 'auto', border: '1px solid gray' }}
            key={task.createdOn}
            onDrop={(e) => { this.onDrop(e, index) }}
            onDragStart={(e) => this.onDragStart(e, index)}
            onDragOver={(e) => this.onDragOver(e, index)}
            draggable
          >
            <TodoItem task={task} onChange={this.onChange} index={index} mailList={this.state.mailList} onDeleteTodo={this.onDeleteTodo}/>
          </div>
        })}
        <div
          style={{ background: 'gray', marginRight: '5px', padding: '5px', display: 'inline-block', height: 'auto', width: '200px', overflow: 'auto', border: '1px solid gray' }}
        >
          <TodoItem task={this.state.newTask} onChange={this.onNewTaskChange} mailList={this.state.mailList}/>
          <button onClick={this.addNewTask}>Add New Todo</button>
        </div>
      </div>
    );
  }
}