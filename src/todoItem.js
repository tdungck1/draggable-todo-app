import SearchableDropdown from "./searchableDropdown"

export const TodoItem = ({ task, onChange, index, mailList, onDeleteTodo }) => {
  function onDataChange(e, fieldName) {
    console.log(e)
    onChange({ ...task, [fieldName]: e.target.value }, index)
  }
  const debounceDataChange = debounce(onDataChange, 200)
  return <form>
    <div>title: <textarea defaultValue={task?.title} onChange={(e) => debounceDataChange(e, 'title')}></textarea></div>
    <div>description: <textarea defaultValue={task?.description} onChange={(e) => debounceDataChange(e, 'description')}></textarea></div>
    <div>assignedTo: <SearchableDropdown mailList={mailList} defaultValue={task?.assignedTo} onChange={(e) => debounceDataChange(e, 'assignedTo')}></SearchableDropdown></div>
    <div>dueDate: <input defaultValue={task?.dueDate} onChange={(e) => debounceDataChange(e, 'dueDate')}></input></div>
    <label htmlFor="status">status: </label>
    <select name="status" defaultValue={task?.status} onChange={(e) => debounceDataChange(e, 'status')}>
      <option value="open">open</option>
      <option value="close">close</option>
    </select>
    {index !== undefined && <button onClick={(e) => {e.preventDefault(); onDeleteTodo(index)}}>delete Todo item</button>}
  </form>
}

function debounce(func, timeout) {
  let timeoutId = null
  return function (...arg) {
    if (timeoutId) clearTimeout(timeoutId)
    timeoutId = setTimeout(() => func(...arg), timeout)
  }
}